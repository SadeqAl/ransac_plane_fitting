﻿//I/O Stream
#include <iostream>
#include <string>
#include "ros/ros.h"

//plane fitting
#include "ransac_plane_fitting.h"

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "ransac_plane_fitting_node");
    ros::NodeHandle n;
    double frequency;
    ros::param::get("~frequency", frequency);
    if(frequency == 0)
        frequency = 30;
    ransac_plane_fitting ransac_plane_fitting_obj;
    //Open!
    ransac_plane_fitting_obj.setUp();
    ros::Rate r(frequency);
    //Loop -> Ashyncronous Module
    while(ros::ok())
    {
        ros::spinOnce();
        ransac_plane_fitting_obj.run();
        r.sleep();
    }
    return 1;
}
