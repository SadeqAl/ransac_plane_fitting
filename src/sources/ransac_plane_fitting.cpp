#include "ransac_plane_fitting.h"

ransac_plane_fitting::ransac_plane_fitting() {}
ransac_plane_fitting::~ransac_plane_fitting() {}

void ransac_plane_fitting::ownSetUp()
{
  received_sim_data_ = false;
  // Initializing the filter
  this->init();
}

void ransac_plane_fitting::ownStart()
{
  gazebo_pose_sub_ = n_.subscribe("/gazebo/model_states", 1, &ransac_plane_fitting::PointsCallback, this);
  Lemniscate_tra_pub_ = n_.advertise<visualization_msgs::Marker>(drone_sim_pose_, 1);
  plane_fitting_pub_ = n_.advertise<visualization_msgs::MarkerArray>(plane_fitting_, 1);
  plane_fitting_rndm_pub_ = n_.advertise<visualization_msgs::MarkerArray>(plane_rndm_fitting_, 1);
}

void ransac_plane_fitting::ownStop()
{
  received_sim_data_ = false;
}

void ransac_plane_fitting::ownRun()
{
  if(received_sim_data_)
  {
    RANSAC(plane_points_cloud_);
  }
}

void ransac_plane_fitting::init()
{
  ros::param::get("~stack_path", stack_path_);

  if(stack_path_.length() == 0)
    stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

  ros::param::get("~drone_id", drone_id_);

  ros::param::get("~drone_sim_pose", drone_sim_pose_);

  if(drone_sim_pose_.length() == 0)
    drone_sim_pose_ = "/gazebo/model_states";

  ros::param::get("~plane_fitting", plane_fitting_);

  if(plane_fitting_.length() == 0)
    plane_fitting_ = "plane_fitting";

  ros::param::get("~plane_rndm_fitting", plane_rndm_fitting_);

  if(plane_rndm_fitting_.length() == 0)
    plane_rndm_fitting_ = "plane_rndm_fitting";

  plane_points_cloud_.reset(new pcl::PointCloud<pcl::PointXYZ>);
  plane_points_vector_.clear();
}

void ransac_plane_fitting::display_mav_tra(pcl::PointXYZ mav_pose)
{
  geometry_msgs::Point mav_points;
  mav_marker_.header.stamp = ros::Time();
  mav_marker_.header.frame_id = "world";
  mav_marker_.ns = "MAV_TRA";
  mav_marker_.id = 200;
  mav_marker_.type = visualization_msgs::Marker::LINE_STRIP;
  mav_marker_.action = visualization_msgs::Marker::ADD;
  mav_marker_.pose.orientation.x = 0.0;
  mav_marker_.pose.orientation.y = 0.0;
  mav_marker_.pose.orientation.z = 0.0;
  mav_marker_.pose.orientation.w = 1;
  mav_marker_.scale.x = 0.2;
  mav_marker_.color.a = 1.0;
  mav_marker_.color.r = 1;
  mav_marker_.color.g = 0;
  mav_marker_.color.b = 0;
  mav_points.x = mav_pose.x;
  mav_points.y = mav_pose.y;
  mav_points.z = mav_pose.z;
  mav_marker_.points.push_back(mav_points);
  Lemniscate_tra_pub_.publish(mav_marker_);
}

void ransac_plane_fitting::display_plane_points(pcl::PointIndices::Ptr inliers,
                                                pcl::ModelCoefficients::Ptr coefficients,
                                                pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
  visualization_msgs::Marker plane_marker;
  visualization_msgs::Marker rndm_marker;
  int marker_id = 0;

  for (std::size_t i = 0; i < inliers->indices.size(); i++)
  {
    plane_marker.header.stamp = ros::Time();
    plane_marker.header.frame_id = "world";
    plane_marker.ns = "PLANE_FITTING";
    plane_marker.id = marker_id;
    plane_marker.type = visualization_msgs::Marker::CUBE;
    plane_marker.action = visualization_msgs::Marker::ADD;
    plane_marker.pose.position.x = cloud->points[inliers->indices[i]].x;
    plane_marker.pose.position.y = cloud->points[inliers->indices[i]].y;
    plane_marker.pose.position.z = cloud->points[inliers->indices[i]].z;
    plane_marker.pose.orientation.x = 0.0;
    plane_marker.pose.orientation.y = 0.0;
    plane_marker.pose.orientation.z = 0.0;
    plane_marker.pose.orientation.w = 1.0;
    plane_marker.scale.x = 2;
    plane_marker.scale.y = 2;
    plane_marker.scale.z = 2;
    plane_marker.color.a = 1.0;
    plane_marker.color.r = 0.0;
    plane_marker.color.g = 1.0;
    plane_marker.color.b = 0.0;

    // Plane points' coeffiecients
    double a = coefficients->values[0];
    double b = coefficients->values[1];
    double c = coefficients->values[2];
    double d = coefficients->values[3];

    for (int i = 0; i < 100; i++)
    {
      rndm_marker.header.stamp = ros::Time();
      rndm_marker.header.frame_id = "world";
      rndm_marker.ns = "PLANE_FITTING_RNDM";
      rndm_marker.id = i;
      rndm_marker.type = visualization_msgs::Marker::CUBE;
      rndm_marker.action = visualization_msgs::Marker::ADD;
      rndm_marker.pose.orientation.x = 0.0;
      rndm_marker.pose.orientation.y = 0.0;
      rndm_marker.pose.orientation.z = 0.0;
      rndm_marker.pose.orientation.w = 1.0;
      rndm_marker.pose.position.x = rand() % + 100 - 50;
      rndm_marker.pose.position.y = rand() % + 100 - 50;
      rndm_marker.pose.position.z = (-d - a *  rndm_marker.pose.position.x
                                     - b * rndm_marker.pose.position.y) / c;
      rndm_marker.scale.x = 2;
      rndm_marker.scale.y = 2;
      rndm_marker.scale.z = 2;
      rndm_marker.color.a = 1.0;
      rndm_marker.color.r = 0.0;
      rndm_marker.color.g = 0.0;
      rndm_marker.color.b = 1.0;
      rndm_marker_array_.markers.push_back(rndm_marker);
    }

    plane_marker_array_.markers.push_back(plane_marker);
    marker_id ++;
  }
  plane_fitting_pub_.publish(plane_marker_array_);
  plane_fitting_rndm_pub_.publish(rndm_marker_array_);
  rndm_marker_array_.markers.clear();
  plane_marker_array_.markers.clear();
}

void ransac_plane_fitting::PointsCallback(const gazebo_msgs::ModelStates::ConstPtr &msg)
{
  for (int i = 0; i < msg->name.size(); i++)
  {
    if (msg->name[i].compare("moving_drone") == 0)
    {
      object_state_.stamp_ = ros::Time::now();
      mav_pose_.x = msg->pose[i].position.x;
      mav_pose_.y = msg->pose[i].position.y;
      mav_pose_.z = msg->pose[i].position.z;
      received_sim_data_ = true;
      plane_points_vector_.push_back(mav_pose_);

      if(plane_points_vector_.size() > 49)
        plane_points_vector_.erase(plane_points_vector_.begin());
    }
    break;
  }

  for (int i = 0; i < plane_points_vector_.size(); i++)
  {
    plane_points_cloud_->points.push_back(plane_points_vector_[i]);
  }

  display_mav_tra(mav_pose_);
  if(mav_marker_.points.size() > 1600)
    mav_marker_.points.clear();
}

// RANSAC main functino
int ransac_plane_fitting::RANSAC(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
  if(cloud->size() < 5)
    return 0;

  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold(0.5);
  seg.setInputCloud (cloud);
  seg.setMaxIterations(50);
  seg.segment (*inliers, *coefficients);

  if (!inliers->indices.empty())
    display_plane_points(inliers, coefficients, cloud);
  plane_points_cloud_->points.clear();
  return 1;
}
