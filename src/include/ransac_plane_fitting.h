#ifndef aukf_tra_pre_H
#define aukf_tra_pre_H

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include "cstdlib"

//opencv
#include <opencv/cv.h>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/core/core.hpp"

//Drone module
#include "robot_process.h"

//ROS message
#include "ros/ros.h"
#include "nav_msgs/Path.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PointStamped.h>

//DroneMsgsROS
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>
#include <droneMsgsROS/droneTrajectoryRefCommand.h>
#include <droneMsgsROS/droneRefCommand.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/robotPoseStampedVector.h>
#include <droneMsgsROS/robotPoseStamped.h>
#include <nav_msgs/Path.h>

//Gazebo messages
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/ModelState.h"
#include "gazebo_msgs/SetModelState.h"

//PCL
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

class ransac_plane_fitting : public RobotProcess
{
    public:
    ransac_plane_fitting();
    ~ransac_plane_fitting();

    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();
    void display_plane_points(pcl::PointXYZ plane_points);
    void display_mav_tra(cv::Point3d mav_pose);
    void PointsCallback(const gazebo_msgs::ModelStates::ConstPtr &msg);
    void display_mav_tra(pcl::PointXYZ mav_pose);
    void display_plane_points(pcl::PointIndices::Ptr inliers,
                              pcl::ModelCoefficients::Ptr coefficients,
                              pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    int RANSAC(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    int loop_counter_ = 49;
    bool received_sim_data_;

   private:

    struct {
        ros::Time stamp_;
        double pose_x_, pose_y_, pose_z_;
    }object_state_;

    std::string stack_path_;
    std::string drone_id_;
    std::string drone_sim_pose_;
    std::string plane_fitting_;
    std::string plane_rndm_fitting_;

    ros::NodeHandle n_;
    ros::Subscriber gazebo_pose_sub_;
    ros::Publisher Lemniscate_tra_pub_, plane_fitting_pub_, plane_fitting_rndm_pub_;

    visualization_msgs::Marker mav_marker_;
    visualization_msgs::MarkerArray plane_marker_array_;
    visualization_msgs::MarkerArray rndm_marker_array_;

    pcl::PointCloud<pcl::PointXYZ>::Ptr plane_points_cloud_;
    std::vector<pcl::PointXYZ> plane_points_vector_;
    pcl::PointXYZ mav_pose_;
};

#endif
