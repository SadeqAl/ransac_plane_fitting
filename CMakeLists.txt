cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME RANSAC_PLANE_FITTING)
project(${PROJECT_NAME})

set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -Wall -g")
set(RANSAC_PLANE_FITTING_SOURCE_DIR
    src/sources)
set(RANSAC_PLANE_FITTING_INCLUDE_DIR
    src/include)
set(RANSAC_PLANE_FITTING_SOURCE_FILES
    ${RANSAC_PLANE_FITTING_SOURCE_DIR}/ransac_plane_fitting.cpp)
set(RANSAC_PLANE_FITTING_HEADER_FILES
    ${RANSAC_PLANE_FITTING_INCLUDE_DIR}/ransac_plane_fitting.h)
# pcl 1.7 causes a segfault when it is built with debug mode
set(CMAKE_BUILD_TYPE "RELEASE")

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  rospy  
  robot_process
  droneMsgsROS
  opencv_apps
  geometry_msgs
  sensor_msgs
  nav_msgs
  visualization_msgs
  message_generation
  cv_bridge
  image_transport
  tf_conversions
  tf message_generation
  pcl_ros
  pcl_msgs
  pcl_conversions
)

find_package(PCL 1.7 REQUIRED
    COMPONENTS common io sample_consensus segmentation filters)

FIND_PACKAGE(Cholmod)
include_directories(${CHOLMOD_INCLUDE_DIR})

find_package(OpenCV REQUIRED)

find_package(Eigen3)
if(NOT EIGEN3_FOUND)
    # Fallback to cmake_modules
    find_package(cmake_modules REQUIRED)
    find_package(Eigen REQUIRED)
    set(EIGEN3_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
    set(EIGEN3_LIBRARIES ${EIGEN_LIBRARIES})  # Not strictly necessary as Eigen is head only
    # Possibly map additional variables to the EIGEN3_ prefix.
    message(WARN "Using Eigen2!")
else()
    set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
endif()

generate_messages(
  DEPENDENCIES
  std_msgs
  sensor_msgs
  geometry_msgs
  )

catkin_package(
    INCLUDE_DIRS ${RANSAC_PLANE_FITTING_INCLUDE_DIR}
    LIBRARIES OpenCV PCL
    CATKIN_DEPENDS roscpp std_msgs sensor_msgs image_transport tf_conversions tf
    message_runtime cv_bridge image_transport pcl_ros pcl_msgs pcl_conversions
    )

include_directories(
    ${RANSAC_PLANE_FITTING_INCLUDE_DIR}
    ${catkin_INCLUDE_DIRS}
    ${OpenCV_INCLUDE_DIRS}
    SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIRS}
    ${PCL_INCLUDE_DIRS}
    )

link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_library(RANSAC_PLANE_FITTING_lib ${RANSAC_PLANE_FITTING_SOURCE_FILES} ${RANSAC_PLANE_FITTING_HEADER_FILES})
add_dependencies(RANSAC_PLANE_FITTING_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(RANSAC_PLANE_FITTING_lib ${catkin_LIBRARIES} ${Eigen_LIBRARIES} ${OpenCV_LIBS})
target_link_libraries(RANSAC_PLANE_FITTING_lib ${EIGEN3_LIBRARIES})
target_link_libraries(RANSAC_PLANE_FITTING_lib ${PCL_COMMON_LIBRARIES} ${PCL_IO_LIBRARIES}
    ${PCL_SAMPLE_CONSENSUS_LIBRARIES} ${PCL_SEGMENTATION_LIBRARIES} ${PCL_FILTERS_LIBRARIES})

add_executable(ransac_plane_fitting_node src/sources/ransac_plane_fitting_node.cpp)
add_dependencies(ransac_plane_fitting_node ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(ransac_plane_fitting_node RANSAC_PLANE_FITTING_lib ${OpenCV_LIBRARY})
target_link_libraries(ransac_plane_fitting_node ${catkin_LIBRARIES})
